﻿/*
=============================================
Author      : <ยุทธภูมิ ตวันนา>
Create date : <๒๔/๑๐/๒๕๖๑>
Modify date : <๑๒/๐๓/๒๕๖๕>
Description : <รวมรวบฟังก์ชั่นใช้งานสำหรับข้อมูลอาจารย์>
=============================================
*/

(function () {
    "use strict";

    angular.module("hri2Mod", [
        "ngTable",
        "utilMod",
        "appMod",
        "dictMod",
        "pageRouteMod",
        "facultyMod",
        "hriMod"
    ])
    
    .service("hri2Serv", function ($rootScope, $timeout, $compile, utilServ, appServ, pageRouteServ, facultyServ) {
        var self = this;

        self.dataSelect = {
            data: []
        };

        self.form = {
            getDialogForm: {
                template: pageRouteServ.pageObject.HRi.template,
                action: function () {
                    if (appServ.isUser && facultyServ.isFaculty) {
                        utilServ.getDialogPreloadingWithDict(["msgPreloading", "loading"]);

                        $timeout(function () {
                            utilServ.http({
                                url: self.form.getDialogForm.template
                            }).then(function (result) {
                                var title = ["HRi", "personalInformation"];
                                var content = $compile($(".template-content").html(result.data).contents())($rootScope);

                                appServ.dialogFormOptions = {
                                    cssClass: "",
                                    title: title,
                                    content: content
                                };
                            });
                        }, 0);
                    }
                }
            }
        };
    })
    
    .controller("hriCtrl", function ($scope, $timeout, $q, $filter, NgTableParams, utilServ, appServ, facultyServ, hriServ, hri2Serv) {
        var self = this;

        self.tableList = new NgTableParams({}, {});

        self.init = function () {
            if (appServ.isUser && facultyServ.isFaculty) {
                self.setValue().then(function () {
                    self.watchFormChange();
                    self.resetValue();
                    self.showForm = true;
                });
            }
            else
                self.showForm = false;
        };

        self.watchFormChange = function () {
            $timeout(function () {
                $scope.$watch(function () {
                    var watch = [];

                    watch.push(self.tableList.settings().$loading);

                    return watch;
                }, function () {
                    if (self.tableList.settings().$loading) {
                        $(".hri table tbody").hide();
                        $(".hri .pagination").hide();
                    }
                    else {
                        $(".hri table tbody").show();
                        $(".hri .pagination").show();

                        self.table.finishRender();
                    }
                }, true);

                $scope.$watch(function () {
                    return [
                        self.table.filter.formField.keyword
                    ];
                }, function (newValue, oldValue) {
                    if (newValue[0] !== oldValue[0]) {
                        var obj = self.table.reload;

                        obj.isPreloading = false;
                        obj.isResetDataSource = false;
                        obj.order = [{
                            isFirstPage: true
                        }];
                        obj.action();
                        self.uncheckboxAll();
                    }
                }, true);

                $scope.$watch(function () {
                    return [
                        self.table.hide
                    ];
                }, function () {
                    if (self.table.hide) {
                        self.table.filter.setValue();
                        self.uncheckboxAll();
                        $(".hri .pagination").hide();
                    }
                    else
                        $(".hri .pagination").show();
                }, true);
            }, 0);
        };

        self.setValue = function () {
            var deferred = $q.defer();

            self.showForm = false;

            self.search.setValue().then(function () {
                self.search.watchFormChange();
                self.table.filter.setValue();
                self.table.getData();

                $timeout(function () {
                    deferred.resolve();
                }, 0);
            });

            return deferred.promise;
        };

        self.resetValue = function () {
        };
        
        self.table = {
            isReload: false,
            hide: true,
            dataSource: [],
            temp: [],
            formField: {
                toggle: {},
                checkboxes: {
                    checked: false,
                    items: {}
                }
            },
            filter: {
                formField: {
                    keyword: ""
                },
                setValue: function () {
                    this.resetValue();
                    this.showForm = false;
                },
                resetValue: function () {
                    this.formField.keyword = "";
                },
                action: function (dataTable) {
                    return $filter("filter")(dataTable, {
                        selectFilter: (this.formField.keyword ? this.formField.keyword : "!null")
                    });
                }
            },
            getData: function () {
                self.tableList = new NgTableParams(appServ.tableConfig.params, angular.extend(appServ.tableConfig.setting, {
                    getData: function (params) {
                        return $timeout(function () { }, appServ.tableConfig.timeout).then(function (result) {
                            return self.table.reload.getData().then(function () {
                                var dt = self.table.dataSource;
                                var df = self.table.filter.action(dt);
                                var data = df.slice((params.page() - 1) * params.count(), params.page() * params.count());

                                self.table.temp = df;

                                params.total(df.length);
                                params.totalSearch = dt.length;
                                params.totalPage = (Math.ceil(params.total() / params.count()));

                                return data;
                            });
                        });
                    }
                }));
            },
            finishRender: function () {
                appServ.closeDialogPreloading();

                $timeout(function () {
                    if ($("#" + utilServ.idDialogForm).is(":visible") === false) {
                        utilServ.dialogFormWithDict(appServ.dialogFormOptions, function (e) {
                        });
                    }
                }, 0);
            },
            reload: {
                isPreloading: false,
                isResetDataSource: false,
                order: [],
                action: function () {
                    var deferred = $q.defer();

                    if (this.isPreloading)
                        utilServ.getDialogPreloadingWithDict(["msgPreloading", "loading"]);

                    if (this.isResetDataSource)
                        self.table.dataSource = [];

                    this.getData().then(function () {
                        angular.forEach(self.table.reload.order, function (item) {
                            self.tableList.reload();

                            if (self.tableList.total() === 0)
                                item.isFirstPage = true;

                            if (item.isFirstPage)
                                self.tableList.page(1);
                        });

                        $timeout(function () {
                            deferred.resolve();
                        }, 0);
                    });

                    return deferred.promise;
                },
                getData: function () {
                    var deferred = $q.defer();

                    if (self.table.isReload) {
                        self.table.isReload = false;

                        if (self.search.getValue().firstName || self.search.getValue().lastName) {
                            hriServ.getDataSource({
                                action: "getlist",
                                params: {
                                    firstName: self.search.getValue().firstName,
                                    lastName: self.search.getValue().lastName
                                },
                                query: [
                                    "",
                                    "addPosition=Y"
                                ].join("&")
                            }).then(function (result) {
                                self.table.dataSource = result;

                                $timeout(function () {
                                    deferred.resolve();
                                }, 0);
                            });
                        }
                        else
                            $timeout(function () {
                                deferred.resolve();
                            }, 0);
                    }
                    else
                        $timeout(function () {
                            deferred.resolve();
                        }, 0);

                    return deferred.promise;
                }
            }
        };

        self.checkboxRootOnOffByChild = function () {
            self.table.formField.checkboxes.checked = utilServ.uncheckboxRoot({
                data: self.table.temp,
                checkboxesRoot: self.table.formField.checkboxes.checked,
                checkboxesChild: self.table.formField.checkboxes.items,
                field: "id"
            });
        };

        self.checkboxChildOnOffByRoot = function () {
            utilServ.uncheckboxAll({
                data: self.table.temp,
                checkboxesRoot: self.table.formField.checkboxes.checked,
                checkboxesChild: self.table.formField.checkboxes.items,
                field: "id"
            });
        };

        self.uncheckboxAll = function () {
            if ($(".hri .table .inputcheckbox:checked").length > 0) {
                if (self.table.formField.checkboxes.checked)
                    self.table.formField.checkboxes.checked = false;

                self.checkboxChildOnOffByRoot();
            }
        };

        self.search = {
            formField: {
                firstName: "",
                lastName: ""
            },
            watchFormChange: function () {
                $timeout(function () {
                    $scope.$watch(function () {
                        var watch = [];

                        watch.push(self.search.formField.firstName);
                        watch.push(self.search.formField.lastName);

                        return watch;
                    }, function (newValue, oldValue) {
                        var exit = false;

                        if (!exit) {
                            if ((newValue[0] || newValue[1]) &&
                                ((newValue[0] !== oldValue[0]) ||
                                 (newValue[1] !== oldValue[1])))
                                exit = true;
                        }

                        self.search.isFormChanged = exit;
                        self.table.hide = true;
                    }, true);
                }, 0);
            },
            setValue: function () {
                var deferred = $q.defer();

                this.resetValue();
                this.showForm = false;

                this.isFormChanged = false;

                $timeout(function () {
                    deferred.resolve();
                }, 0);

                return deferred.promise;
            },
            resetValue: function () {
                this.formField.firstName = "";
                this.formField.lastName = "";

                self.table.dataSource = [];
            },
            getValue: function () {
                var result = {
                    firstName: (this.formField.firstName ? this.formField.firstName : ""),
                    lastName: (this.formField.lastName ? this.formField.lastName : "")
                };

                return result;
            },
            action: function () {
                utilServ.getDialogPreloadingWithDict(["msgPreloading", "searching"]);

                self.uncheckboxAll();

                self.table.hide = true
                self.table.isReload = true;
                self.table.reload.isPreloading = true;
                self.table.reload.isResetDataSource = true;
                self.table.reload.order = [{
                    isFirstPage: true
                }];
                self.table.reload.action().then(function () {
                    self.table.hide = false;
                });
            }
        };

        self.saveChange = {
            validate: function () {
                if (utilServ.getValueCheckboxTrue(self.table.temp, "id", self.table.formField.checkboxes.items).length === 0) {
                    utilServ.dialogErrorWithDict(["entries", "selectError"], function () {
                    });

                    return false;
                }

                return true;
            },
            action: function () {
                if (this.validate()) {
                    var data = [];

                    angular.forEach(self.table.temp, function (item) {
                        if (self.table.formField.checkboxes.items[item.id])
                            data.push(item);
                    });

                    angular.copy(data, hri2Serv.dataSelect.data);

                    $("#" + utilServ.idDialogForm + ".modal").modal("hide");
                }
            }
        };
    });
})();