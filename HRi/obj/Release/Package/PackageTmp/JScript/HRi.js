﻿/*
=============================================
Author      : <ยุทธภูมิ ตวันนา>
Create date : <๒๓/๐๙/๒๕๖๒>
Modify date : <๑๒/๐๓/๒๕๖๕>
Description : <รวมรวบฟังก์ชั่นใช้งานสำหรับข้อมูลบุคลากร>
=============================================
*/

(function () {
    "use strict";

    angular.module("hriMod", [
        "utilMod"
    ])

    .service("hriServ", function ($q, $filter, utilServ) {
        var self = this;

        self.getDataSource = function (param) {
            param.action = (param.action === undefined ? "" : param.action);
            param.params = (param.params === undefined || param.params === "" ? {} : param.params);
            param.query = (param.query === undefined || param.query === "" ? "" : param.query);

            var deferred = $q.defer();
            var url = (utilServ.getURLAPI(self.pathAPI) + "HRi/");
            var route = "";

            switch (param.action) {
                case "getlist":
                    route = "GetListData";
                    break;
                case "get":
                    route = "GetData";
                    break;
            }

            url += (route + "?ver=" + utilServ.dateTimeOnURL + param.query);

            utilServ.http({
                url: url,
                method: "POST",
                data: param.params
            }).then(function (result) {
                var dt = [];
                var positions = [];
                var programs = [];
                var educations = [];
                var position = null;

                angular.forEach(result.data.data.content, function (item) {
                    if (param.action === "getlist") {
                        position = null;

                        if (item.positions && item.positions.length > 0)
                            position = item.positions[item.positions.length - 1];

                        dt.push({
                            dataSource: "HRi",
                            id: (item.personalId ? item.personalId : ""),
                            personalId: (item.personalId ? item.personalId : ""),
                            firstName: {
                                TH: (item.firstName ? item.firstName : ""),
                                EN: (item.firstNameEn ? $filter("capitalize")(item.firstNameEn) : "")
                            },
                            lastName: {
                                TH: (item.lastName ? item.lastName : ""),
                                EN: (item.lastNameEn ? $filter("capitalize")(item.lastNameEn) : "")
                            },
                            fullName: {
                                TH: ((item.firstName ? $filter("capitalize")(item.firstName) : "") + (item.lastName ? ((item.firstName ? " " : "") + $filter("capitalize")(item.lastName)) : "")),
                                EN: ((item.firstNameEn ? $filter("capitalize")(item.firstNameEn) : "") + (item.lastNameEn ? ((item.firstNameEn ? " " : "") + $filter("capitalize")(item.lastNameEn)) : ""))
                            },
                            positions: item.positions,
                            position: {
                                TH: (position ? (position.fullnameTH ? position.fullnameTH : position.name) : ""),
                                EN: (position ? (position.fullnameEN ? $filter("capitalize")(position.fullnameEN) : "") : ""),
                                name: {
                                    TH: (position ? (position.fullnameTH ? position.fullnameTH : position.name) : ""),
                                    EN: (position ? (position.fullnameEN ? $filter("capitalize")(position.fullnameEN) : (position.fullnameTH ? position.fullnameTH : position.name)) : "")
                                }
                            },
                            department: {
                                TH: (item.department ? item.department : ""),
                                EN: (item.departmentEn ? $filter("capitalize")(item.departmentEn) : ""),
                                name: {
                                    TH: (item.department ? item.department : (item.departmentEn ? $filter("capitalize")(item.departmentEn) : "")),
                                    EN: (item.departmentEn ? $filter("capitalize")(item.departmentEn) : (item.department ? item.department : "")),
                                }
                            },
                            organization: {
                                TH: (position && position.organization ? (position.organization.fullnameTH ? position.organization.fullnameTH : position.organization.name) : ""),
                                EN: (position && position.organization ? (position.organization.fullnameEN ? $filter("capitalize")(position.organization.fullnameEN) : "") : ""),
                                name: {
                                    TH: (position && position.organization ? (position.organization.fullnameTH ? position.organization.fullnameTH : position.organization.name) : ""),
                                    EN: (position && position.organization ? (position.organization.fullnameEN ? $filter("capitalize")(position.organization.fullnameEN) : (position.organization.fullnameTH ? position.organization.fullnameTH : position.organization.name)) : "")
                                }
                            },
                            selectFilter: (
                                (item.firstName ? item.firstName : "") +
                                (item.lastName ? item.lastName : "") +
                                (item.firstNameEn ? item.firstNameEn : "") +
                                (item.lastNameEn ? item.lastNameEn : "") +
                                (item.department ? item.department : "") +
                                (item.departmentEn ? item.departmentEn : "") +
                                (position ? (position.name ? position.name : "") : "") +
                                (position ? (position.fullnameTH ? position.fullnameTH : "") : "") +
                                (position ? (position.fullnameEN ? position.fullnameEN : "") : "") +
                                (position && position.organization ? (position.organization.name ? position.organization.name : "") : "") +
                                (position && position.organization ? (position.organization.fullnameTH ? position.organization.fullnameTH : "") : "") +
                                (position && position.organization ? (position.organization.fullnameEN ? position.organization.fullnameEN : "") : "")
                            )
                        });
                    }

                    if (param.action === "get") {
                        angular.forEach(item.positions, function (position) {
                            positions.push({
                                name: (position.name ? position.name : ""),
                                fullName: {
                                    TH: (position.fullnameTH ? position.fullnameTH : (position.fullnameEN ? $filter("capitalize")(position.fullnameEN) : "")),
                                    EN: (position.fullnameEN ? $filter("capitalize")(position.fullnameEN) : (position.fullnameTH ? position.fullnameTH : ""))
                                },
                                type: (position.type ? position.type : ""),
                                startDate: (position.startDate ? position.startDate : ""),
                                organization: {
                                    name: (position.organization ? (position.organization.name ? position.organization.name : "") : ""),
                                    fullName: {
                                        TH: (position.organization ? (position.organization.fullnameTH ? position.organization.fullnameTH : (position.organization.fullnameEN ? $filter("capitalize")(position.organization.fullnameEN) : "")) : ""),
                                        EN: (position.organization ? (position.organization.fullnameEN ? $filter("capitalize")(position.organization.fullnameEN) : (position.organization.fullnameTH ? position.organization.fullnameTH : "")) : "")
                                    }
                                }
                            });
                        });

                        angular.forEach(item.programs, function (program) {
                            programs.push({
                                id: (program.id ? program.id : ""),
                                faculty: {
                                    id: (program.facultyId ? program.facultyId : ""),
                                    name: {
                                        TH: (program.facultyNameTH ? program.facultyNameTH : ""),
                                        EN: (program.facultyNameEN ? $filter("capitalize")(program.facultyNameEN) : "")
                                    }
                                },
                                programId: (program.programId ? program.programId : ""),
                                programCode: (program.programCode ? program.programCode : ""),
                                programFullCode: ((program.programCode ? (program.programCode + " ") : "") + (program.majorCode ? (program.majorCode + " ") : "") + (program.groupNum ? program.groupNum : "")),
                                majorId: (program.majorId ? program.majorId : ""),
                                majorCode: (program.majorCode ? program.majorCode : ""),
                                groupNum: (program.groupNum ? program.groupNum : ""),
                                name: {
                                    TH: (program.nameTh ? program.nameTh : ""),
                                    EN: (program.nameEn ? $filter("capitalize")(program.nameEn) : "")
                                },
                                courseYear: (program.courseYear ? program.courseYear : ""),
                                cancelStatus: (program.cancelStatus ? program.cancelStatus : ""),
                                coursePosition: {
                                    id: (program.coursePositionId ? program.coursePositionId : ""),
                                    name: {
                                        TH: (program.coursePositionNameTH ? program.coursePositionNameTH : ""),
                                        EN: (program.coursePositionNameEN ? program.coursePositionNameEN : ""),
                                    },
                                    group: (program.coursePositionGroup ? program.coursePositionGroup : "")
                                }
                            });
                        });

                        angular.forEach(item.educations, function (education) {
                            educations.push({
                                educationType: (education.educationType ? education.educationType : ""),
                                institute: (education.institute ? education.institute : ""),
                                training: (education.training ? education.training : ""),
                                country: (education.country ? education.country : ""),
                                status: (education.status ? education.status : ""),
                                branch1: (education.branch1 ? education.branch1 : ""),
                                branch2: (education.branch2 ? education.branch2 : ""),
                                certificate: (education.certificate ? education.certificate : ""),
                                finalGrade: (education.finalGrade ? education.finalGrade : ""),
                                graduateYear: (education.graduateYear ? education.graduateYear : "")
                            });
                        });

                        dt.push({
                            personalId: (item.personalId ? item.personalId : ""),
                            namePrefix: {
                                academicPosition: (item.titleZ ? item.titleZ : ""),
                                military: (item.titleS ? item.titleS : ""),
                                profession: (item.titleV ? item.titleV : ""),
                                titleConferredByTheKing: (item.titleT ? item.titleT : ""),
                                ordinary: (item.title ? item.title : "")
                            },
                            gender: (item.gender ? item.gender : ""),
                            firstName: {
                                TH: (item.firstName ? item.firstName : ""),
                                EN: (item.firstNameEn ? $filter("capitalize")(item.firstNameEn) : "")
                            },
                            middleName: {
                                TH: (item.middleName ? item.middleName : ""),
                                EN: (item.middleNameEn ? $filter("capitalize")(item.middleNameEn) : "")
                            },
                            lastName: {
                                TH: (item.lastName ? item.lastName : ""),
                                EN: (item.lastNameEn ? $filter("capitalize")(item.lastNameEn) : "")
                            },
                            dateOfBirth: (item.birthDate ? item.birthDate : ""),
                            countryOfBirth: (item.birthCountry ? $filter("capitalize")(item.birthCountry) : ""),
                            placeOfBirth: (item.birthPlace ? $filter("capitalize")(item.birthPlace) : ""),
                            nationality: (item.nationality ? $filter("capitalize")(item.nationality) : ""),
                            nationalitySecond: (item.nationalitySecond ? $filter("capitalize")(item.nationalitySecond) : ""),
                            nationalityThird: (item.nationalityThird ? $filter("capitalize")(item.nationalityThird) : ""),
                            religious: (item.religious ? item.religious : ""),
                            maritalStatus: (item.marital ? item.marital : ""),
                            positions: positions,
                            programs: programs,
                            educations: educations
                        });
                    }
                });

                if (param.action === "getlist") {
                    var output = [];
                    var keys = [];

                    angular.forEach(dt, function (item) {
                        var key = item.personalId;

                        if (keys.indexOf(key) === -1) {
                            keys.push(key);
                            output.push(item);
                        }
                    });

                    dt = output;
                }

                deferred.resolve(dt);
            });

            return deferred.promise;
        };
    });
})();